﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OilExpert
{
    public partial class Facts : Form
    {
        public Facts()
        {
            InitializeComponent();
        }

        private void Facts_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'oilDBDataSet.Факт' table. You can move, or remove it, as needed.
            this.фактTableAdapter.Fill(this.oilDBDataSet.Факт);

        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            var wnd = new AddFact();
            wnd.Text = "Добавление факта";
            wnd.buttonOk.Text = "Добавить";
            wnd.comboBoxType.SelectedIndex = 0;
            if (wnd.ShowDialog() == DialogResult.OK)
            {
                фактTableAdapter.Insert(wnd.comboBoxType.Text,
                    wnd.textBoxName.Text, wnd.textBoxDescription.Text);
                фактTableAdapter.Fill(oilDBDataSet.Факт);
                фактBindingSource.ResetBindings(false);
            }
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            int id = Program.Convert<int>(dataGridView1.CurrentRow.Cells[0].Value, 0);
            string type = Program.Convert<string>(dataGridView1.CurrentRow.Cells[1].Value, "");
            string name = Program.Convert<string>(dataGridView1.CurrentRow.Cells[2].Value, "");
            string descr = Program.Convert<string>(dataGridView1.CurrentRow.Cells[3].Value, "");
            var wnd = new AddFact();
            wnd.textBoxName.Text = name;
            wnd.textBoxDescription.Text = descr;
            wnd.comboBoxType.Text = type;
            if (wnd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                фактTableAdapter.Update(wnd.comboBoxType.SelectedText,
                    wnd.textBoxName.Text, wnd.textBoxDescription.Text, id);
                фактTableAdapter.Fill(oilDBDataSet.Факт);
                фактBindingSource.ResetBindings(false);
            }
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Вы действительно хотите удалить факт?", 
                "Подтверждение", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                int id = Program.Convert<int>(dataGridView1.CurrentRow.Cells[0].Value, 0);
                фактTableAdapter.Delete(id);
                фактTableAdapter.Fill(oilDBDataSet.Факт);
                фактBindingSource.ResetBindings(false);
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
