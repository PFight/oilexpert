﻿namespace OilExpert
{
    partial class AddReadoutFact
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelSensorID = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDownUpper = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.numericUpDownLower = new System.Windows.Forms.NumericUpDown();
            this.dataGridViewFacts = new System.Windows.Forms.DataGridView();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.oilDBDataSet = new OilExpert.OilDBDataSet();
            this.oilDBDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.фактBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.фактTableAdapter = new OilExpert.OilDBDataSetTableAdapters.ФактTableAdapter();
            this.iDФактаDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.типDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.названиеDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.описаниеDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownUpper)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLower)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFacts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.oilDBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.oilDBDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.фактBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // labelSensorID
            // 
            this.labelSensorID.AutoSize = true;
            this.labelSensorID.Location = new System.Drawing.Point(79, 9);
            this.labelSensorID.Name = "labelSensorID";
            this.labelSensorID.Size = new System.Drawing.Size(186, 17);
            this.labelSensorID.TabIndex = 10;
            this.labelSensorID.Text = "<идентификатор датчика>";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 17);
            this.label1.TabIndex = 9;
            this.label1.Text = "Датчик:";
            // 
            // numericUpDownUpper
            // 
            this.numericUpDownUpper.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDownUpper.Location = new System.Drawing.Point(545, 43);
            this.numericUpDownUpper.Name = "numericUpDownUpper";
            this.numericUpDownUpper.Size = new System.Drawing.Size(120, 22);
            this.numericUpDownUpper.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(340, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(199, 17);
            this.label3.TabIndex = 12;
            this.label3.Text = "Верхняя граница диапазона:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(195, 17);
            this.label4.TabIndex = 13;
            this.label4.Text = "Нижняя граница диапазона:";
            // 
            // numericUpDownLower
            // 
            this.numericUpDownLower.Location = new System.Drawing.Point(214, 44);
            this.numericUpDownLower.Name = "numericUpDownLower";
            this.numericUpDownLower.Size = new System.Drawing.Size(120, 22);
            this.numericUpDownLower.TabIndex = 14;
            // 
            // dataGridViewFacts
            // 
            this.dataGridViewFacts.AllowUserToAddRows = false;
            this.dataGridViewFacts.AllowUserToDeleteRows = false;
            this.dataGridViewFacts.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewFacts.AutoGenerateColumns = false;
            this.dataGridViewFacts.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewFacts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewFacts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDФактаDataGridViewTextBoxColumn,
            this.типDataGridViewTextBoxColumn,
            this.названиеDataGridViewTextBoxColumn,
            this.описаниеDataGridViewTextBoxColumn});
            this.dataGridViewFacts.DataSource = this.фактBindingSource;
            this.dataGridViewFacts.Location = new System.Drawing.Point(12, 105);
            this.dataGridViewFacts.Name = "dataGridViewFacts";
            this.dataGridViewFacts.ReadOnly = true;
            this.dataGridViewFacts.RowTemplate.Height = 24;
            this.dataGridViewFacts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewFacts.Size = new System.Drawing.Size(700, 249);
            this.dataGridViewFacts.TabIndex = 15;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 85);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(453, 17);
            this.label5.TabIndex = 16;
            this.label5.Text = "Факт, соответствующий заданному диапазону показаний датчика:";
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Location = new System.Drawing.Point(330, 369);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(228, 40);
            this.buttonOk.TabIndex = 17;
            this.buttonOk.Text = "Сохранить";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(580, 369);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(132, 40);
            this.buttonCancel.TabIndex = 18;
            this.buttonCancel.Text = "Отмена";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // oilDBDataSet
            // 
            this.oilDBDataSet.DataSetName = "OilDBDataSet";
            this.oilDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // oilDBDataSetBindingSource
            // 
            this.oilDBDataSetBindingSource.DataSource = this.oilDBDataSet;
            this.oilDBDataSetBindingSource.Position = 0;
            // 
            // фактBindingSource
            // 
            this.фактBindingSource.DataMember = "Факт";
            this.фактBindingSource.DataSource = this.oilDBDataSet;
            // 
            // фактTableAdapter
            // 
            this.фактTableAdapter.ClearBeforeFill = true;
            // 
            // iDФактаDataGridViewTextBoxColumn
            // 
            this.iDФактаDataGridViewTextBoxColumn.DataPropertyName = "IDФакта";
            this.iDФактаDataGridViewTextBoxColumn.HeaderText = "IDФакта";
            this.iDФактаDataGridViewTextBoxColumn.Name = "iDФактаDataGridViewTextBoxColumn";
            this.iDФактаDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDФактаDataGridViewTextBoxColumn.Visible = false;
            // 
            // типDataGridViewTextBoxColumn
            // 
            this.типDataGridViewTextBoxColumn.DataPropertyName = "Тип";
            this.типDataGridViewTextBoxColumn.FillWeight = 70F;
            this.типDataGridViewTextBoxColumn.HeaderText = "Тип";
            this.типDataGridViewTextBoxColumn.Name = "типDataGridViewTextBoxColumn";
            this.типDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // названиеDataGridViewTextBoxColumn
            // 
            this.названиеDataGridViewTextBoxColumn.DataPropertyName = "Название";
            this.названиеDataGridViewTextBoxColumn.FillWeight = 200F;
            this.названиеDataGridViewTextBoxColumn.HeaderText = "Название";
            this.названиеDataGridViewTextBoxColumn.Name = "названиеDataGridViewTextBoxColumn";
            this.названиеDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // описаниеDataGridViewTextBoxColumn
            // 
            this.описаниеDataGridViewTextBoxColumn.DataPropertyName = "Описание";
            this.описаниеDataGridViewTextBoxColumn.HeaderText = "Описание";
            this.описаниеDataGridViewTextBoxColumn.Name = "описаниеDataGridViewTextBoxColumn";
            this.описаниеDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // AddReadoutFact
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(728, 421);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.dataGridViewFacts);
            this.Controls.Add(this.numericUpDownLower);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.numericUpDownUpper);
            this.Controls.Add(this.labelSensorID);
            this.Controls.Add(this.label1);
            this.Name = "AddReadoutFact";
            this.Text = "Редактировать сопоставление показаний фактам";
            this.Load += new System.EventHandler(this.AddReadoutFact_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownUpper)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLower)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFacts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.oilDBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.oilDBDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.фактBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.BindingSource oilDBDataSetBindingSource;
        private OilDBDataSet oilDBDataSet;
        private System.Windows.Forms.BindingSource фактBindingSource;
        private OilDBDataSetTableAdapters.ФактTableAdapter фактTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDФактаDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn типDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn названиеDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn описаниеDataGridViewTextBoxColumn;
        public System.Windows.Forms.Label labelSensorID;
        public System.Windows.Forms.NumericUpDown numericUpDownUpper;
        public System.Windows.Forms.NumericUpDown numericUpDownLower;
        public System.Windows.Forms.DataGridView dataGridViewFacts;
        public System.Windows.Forms.Button buttonOk;

    }
}