﻿namespace OilExpert
{
    partial class ReadoutFact
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.показаниеДатчикаФактViewBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.oilDBDataSet = new OilExpert.OilDBDataSet();
            this.показаниеДатчикаФактBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.buttonRemove = new System.Windows.Forms.Button();
            this.buttonEdit = new System.Windows.Forms.Button();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.labelSensorID = new System.Windows.Forms.Label();
            this.показаниеДатчикаФактTableAdapter = new OilExpert.OilDBDataSetTableAdapters.ПоказаниеДатчикаФактTableAdapter();
            this.показаниеДатчикаФактViewTableAdapter = new OilExpert.OilDBDataSetTableAdapters.ПоказаниеДатчикаФактViewTableAdapter();
            this.iDДатчикаDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.нижняяГраницаДиапазонаDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.верхняяГраницаДиапазонаDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDФактаDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Название = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonОК = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.показаниеДатчикаФактViewBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.oilDBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.показаниеДатчикаФактBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDДатчикаDataGridViewTextBoxColumn,
            this.нижняяГраницаДиапазонаDataGridViewTextBoxColumn,
            this.верхняяГраницаДиапазонаDataGridViewTextBoxColumn,
            this.iDФактаDataGridViewTextBoxColumn,
            this.Название});
            this.dataGridView1.DataSource = this.показаниеДатчикаФактViewBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(12, 55);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(705, 274);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // показаниеДатчикаФактViewBindingSource
            // 
            this.показаниеДатчикаФактViewBindingSource.DataMember = "ПоказаниеДатчикаФактView";
            this.показаниеДатчикаФактViewBindingSource.DataSource = this.oilDBDataSet;
            // 
            // oilDBDataSet
            // 
            this.oilDBDataSet.DataSetName = "OilDBDataSet";
            this.oilDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // показаниеДатчикаФактBindingSource
            // 
            this.показаниеДатчикаФактBindingSource.DataMember = "ПоказаниеДатчикаФакт";
            this.показаниеДатчикаФактBindingSource.DataSource = this.oilDBDataSet;
            // 
            // buttonRemove
            // 
            this.buttonRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonRemove.Location = new System.Drawing.Point(294, 349);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(97, 28);
            this.buttonRemove.TabIndex = 6;
            this.buttonRemove.Text = "Удалить";
            this.buttonRemove.UseVisualStyleBackColor = true;
            this.buttonRemove.Click += new System.EventHandler(this.buttonRemove_Click);
            // 
            // buttonEdit
            // 
            this.buttonEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonEdit.Location = new System.Drawing.Point(159, 349);
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Size = new System.Drawing.Size(128, 28);
            this.buttonEdit.TabIndex = 5;
            this.buttonEdit.Text = "Редактировать";
            this.buttonEdit.UseVisualStyleBackColor = true;
            this.buttonEdit.Click += new System.EventHandler(this.buttonEdit_Click);
            // 
            // buttonAdd
            // 
            this.buttonAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonAdd.Location = new System.Drawing.Point(11, 349);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(142, 28);
            this.buttonAdd.TabIndex = 4;
            this.buttonAdd.Text = "Добавить";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 17);
            this.label1.TabIndex = 7;
            this.label1.Text = "Датчик:";
            // 
            // labelSensorID
            // 
            this.labelSensorID.AutoSize = true;
            this.labelSensorID.Location = new System.Drawing.Point(79, 19);
            this.labelSensorID.Name = "labelSensorID";
            this.labelSensorID.Size = new System.Drawing.Size(186, 17);
            this.labelSensorID.TabIndex = 8;
            this.labelSensorID.Text = "<идентификатор датчика>";
            // 
            // показаниеДатчикаФактTableAdapter
            // 
            this.показаниеДатчикаФактTableAdapter.ClearBeforeFill = true;
            // 
            // показаниеДатчикаФактViewTableAdapter
            // 
            this.показаниеДатчикаФактViewTableAdapter.ClearBeforeFill = true;
            // 
            // iDДатчикаDataGridViewTextBoxColumn
            // 
            this.iDДатчикаDataGridViewTextBoxColumn.DataPropertyName = "IDДатчика";
            this.iDДатчикаDataGridViewTextBoxColumn.HeaderText = "Датчик";
            this.iDДатчикаDataGridViewTextBoxColumn.Name = "iDДатчикаDataGridViewTextBoxColumn";
            this.iDДатчикаDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDДатчикаDataGridViewTextBoxColumn.Visible = false;
            // 
            // нижняяГраницаДиапазонаDataGridViewTextBoxColumn
            // 
            this.нижняяГраницаДиапазонаDataGridViewTextBoxColumn.DataPropertyName = "НижняяГраницаДиапазона";
            this.нижняяГраницаДиапазонаDataGridViewTextBoxColumn.FillWeight = 50F;
            this.нижняяГраницаДиапазонаDataGridViewTextBoxColumn.HeaderText = "От";
            this.нижняяГраницаДиапазонаDataGridViewTextBoxColumn.Name = "нижняяГраницаДиапазонаDataGridViewTextBoxColumn";
            this.нижняяГраницаДиапазонаDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // верхняяГраницаДиапазонаDataGridViewTextBoxColumn
            // 
            this.верхняяГраницаДиапазонаDataGridViewTextBoxColumn.DataPropertyName = "ВерхняяГраницаДиапазона";
            this.верхняяГраницаДиапазонаDataGridViewTextBoxColumn.FillWeight = 50F;
            this.верхняяГраницаДиапазонаDataGridViewTextBoxColumn.HeaderText = "До";
            this.верхняяГраницаДиапазонаDataGridViewTextBoxColumn.Name = "верхняяГраницаДиапазонаDataGridViewTextBoxColumn";
            this.верхняяГраницаДиапазонаDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // iDФактаDataGridViewTextBoxColumn
            // 
            this.iDФактаDataGridViewTextBoxColumn.DataPropertyName = "IDФакта";
            this.iDФактаDataGridViewTextBoxColumn.HeaderText = "IDФакта";
            this.iDФактаDataGridViewTextBoxColumn.Name = "iDФактаDataGridViewTextBoxColumn";
            this.iDФактаDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDФактаDataGridViewTextBoxColumn.Visible = false;
            // 
            // Название
            // 
            this.Название.DataPropertyName = "Название";
            this.Название.FillWeight = 200F;
            this.Название.HeaderText = "Название факта";
            this.Название.Name = "Название";
            this.Название.ReadOnly = true;
            // 
            // buttonОК
            // 
            this.buttonОК.Location = new System.Drawing.Point(606, 349);
            this.buttonОК.Name = "buttonОК";
            this.buttonОК.Size = new System.Drawing.Size(111, 28);
            this.buttonОК.TabIndex = 9;
            this.buttonОК.Text = "ОК";
            this.buttonОК.UseVisualStyleBackColor = true;
            this.buttonОК.Click += new System.EventHandler(this.buttonОК_Click);
            // 
            // ReadoutFact
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(727, 389);
            this.Controls.Add(this.buttonОК);
            this.Controls.Add(this.labelSensorID);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonRemove);
            this.Controls.Add(this.buttonEdit);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.dataGridView1);
            this.Name = "ReadoutFact";
            this.Text = "Сопоставление показаний датчиков фактам";
            this.Load += new System.EventHandler(this.ReadoutFact_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.показаниеДатчикаФактViewBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.oilDBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.показаниеДатчикаФактBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button buttonRemove;
        private System.Windows.Forms.Button buttonEdit;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelSensorID;
        private OilDBDataSet oilDBDataSet;
        private System.Windows.Forms.BindingSource показаниеДатчикаФактBindingSource;
        private OilDBDataSetTableAdapters.ПоказаниеДатчикаФактTableAdapter показаниеДатчикаФактTableAdapter;
        private System.Windows.Forms.BindingSource показаниеДатчикаФактViewBindingSource;
        private OilDBDataSetTableAdapters.ПоказаниеДатчикаФактViewTableAdapter показаниеДатчикаФактViewTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDДатчикаDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn нижняяГраницаДиапазонаDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn верхняяГраницаДиапазонаDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDФактаDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Название;
        private System.Windows.Forms.Button buttonОК;
    }
}