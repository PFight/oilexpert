﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OilExpert
{
    public partial class LogWnd : Form
    {
        public LogWnd()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void LogWnd_Load(object sender, EventArgs e)
        {
            textBoxLog.Text = DeductionMachine.LastLog;
        }
    }
}
