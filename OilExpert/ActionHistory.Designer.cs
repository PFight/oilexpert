﻿namespace OilExpert
{
    partial class ActionHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDФактаDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.логинОператораDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.информацияОПроведенииDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.названиеМероприятияDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.описаниеМероприятияDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.проведенноеМероприятиеViewBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.oilDBDataSet = new OilExpert.OilDBDataSet();
            this.проведенноеМероприятиеBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dateTimePickerFrom = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboBoxOperator = new System.Windows.Forms.ComboBox();
            this.пользовательBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.checkBoxOperator = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dateTimePickerTo = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBoxTime = new System.Windows.Forms.CheckBox();
            this.проведенноеМероприятиеTableAdapter = new OilExpert.OilDBDataSetTableAdapters.ПроведенноеМероприятиеTableAdapter();
            this.проведенноеМероприятиеViewTableAdapter = new OilExpert.OilDBDataSetTableAdapters.ПроведенноеМероприятиеViewTableAdapter();
            this.пользовательTableAdapter = new OilExpert.OilDBDataSetTableAdapters.ПользовательTableAdapter();
            this.buttonOK = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.проведенноеМероприятиеViewBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.oilDBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.проведенноеМероприятиеBindingSource)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.пользовательBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.iDФактаDataGridViewTextBoxColumn,
            this.логинОператораDataGridViewTextBoxColumn,
            this.информацияОПроведенииDataGridViewTextBoxColumn,
            this.названиеМероприятияDataGridViewTextBoxColumn,
            this.описаниеМероприятияDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.проведенноеМероприятиеViewBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(658, 267);
            this.dataGridView1.TabIndex = 16;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "ДатаИВремя";
            this.dataGridViewTextBoxColumn1.FillWeight = 50F;
            this.dataGridViewTextBoxColumn1.HeaderText = "Время";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // iDФактаDataGridViewTextBoxColumn
            // 
            this.iDФактаDataGridViewTextBoxColumn.DataPropertyName = "IDФакта";
            this.iDФактаDataGridViewTextBoxColumn.HeaderText = "IDФакта";
            this.iDФактаDataGridViewTextBoxColumn.Name = "iDФактаDataGridViewTextBoxColumn";
            this.iDФактаDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDФактаDataGridViewTextBoxColumn.Visible = false;
            // 
            // логинОператораDataGridViewTextBoxColumn
            // 
            this.логинОператораDataGridViewTextBoxColumn.DataPropertyName = "ЛогинОператора";
            this.логинОператораDataGridViewTextBoxColumn.FillWeight = 50F;
            this.логинОператораDataGridViewTextBoxColumn.HeaderText = "Оператор";
            this.логинОператораDataGridViewTextBoxColumn.Name = "логинОператораDataGridViewTextBoxColumn";
            this.логинОператораDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // информацияОПроведенииDataGridViewTextBoxColumn
            // 
            this.информацияОПроведенииDataGridViewTextBoxColumn.DataPropertyName = "ИнформацияОПроведении";
            this.информацияОПроведенииDataGridViewTextBoxColumn.HeaderText = "Информация о проведении";
            this.информацияОПроведенииDataGridViewTextBoxColumn.Name = "информацияОПроведенииDataGridViewTextBoxColumn";
            this.информацияОПроведенииDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // названиеМероприятияDataGridViewTextBoxColumn
            // 
            this.названиеМероприятияDataGridViewTextBoxColumn.DataPropertyName = "НазваниеМероприятия";
            this.названиеМероприятияDataGridViewTextBoxColumn.HeaderText = "Мероприятие";
            this.названиеМероприятияDataGridViewTextBoxColumn.Name = "названиеМероприятияDataGridViewTextBoxColumn";
            this.названиеМероприятияDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // описаниеМероприятияDataGridViewTextBoxColumn
            // 
            this.описаниеМероприятияDataGridViewTextBoxColumn.DataPropertyName = "ОписаниеМероприятия";
            this.описаниеМероприятияDataGridViewTextBoxColumn.HeaderText = "Описание";
            this.описаниеМероприятияDataGridViewTextBoxColumn.Name = "описаниеМероприятияDataGridViewTextBoxColumn";
            this.описаниеМероприятияDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // проведенноеМероприятиеViewBindingSource
            // 
            this.проведенноеМероприятиеViewBindingSource.DataMember = "ПроведенноеМероприятиеView";
            this.проведенноеМероприятиеViewBindingSource.DataSource = this.oilDBDataSet;
            // 
            // oilDBDataSet
            // 
            this.oilDBDataSet.DataSetName = "OilDBDataSet";
            this.oilDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // проведенноеМероприятиеBindingSource
            // 
            this.проведенноеМероприятиеBindingSource.DataMember = "ПроведенноеМероприятие";
            this.проведенноеМероприятиеBindingSource.DataSource = this.oilDBDataSet;
            // 
            // dateTimePickerFrom
            // 
            this.dateTimePickerFrom.Location = new System.Drawing.Point(61, 69);
            this.dateTimePickerFrom.Name = "dateTimePickerFrom";
            this.dateTimePickerFrom.Size = new System.Drawing.Size(161, 22);
            this.dateTimePickerFrom.TabIndex = 18;
            this.dateTimePickerFrom.ValueChanged += new System.EventHandler(this.buttonApply_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.comboBoxOperator);
            this.groupBox1.Controls.Add(this.checkBoxOperator);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.dateTimePickerTo);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.checkBoxTime);
            this.groupBox1.Controls.Add(this.dateTimePickerFrom);
            this.groupBox1.Location = new System.Drawing.Point(675, 14);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(246, 230);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Фильтр";
            // 
            // comboBoxOperator
            // 
            this.comboBoxOperator.DataSource = this.пользовательBindingSource;
            this.comboBoxOperator.DisplayMember = "Логин";
            this.comboBoxOperator.FormattingEnabled = true;
            this.comboBoxOperator.Location = new System.Drawing.Point(22, 189);
            this.comboBoxOperator.Name = "comboBoxOperator";
            this.comboBoxOperator.Size = new System.Drawing.Size(212, 24);
            this.comboBoxOperator.TabIndex = 34;
            this.comboBoxOperator.ValueMember = "Логин";
            this.comboBoxOperator.SelectedValueChanged += new System.EventHandler(this.buttonApply_Click);
            // 
            // пользовательBindingSource
            // 
            this.пользовательBindingSource.DataMember = "Пользователь";
            this.пользовательBindingSource.DataSource = this.oilDBDataSet;
            // 
            // checkBoxOperator
            // 
            this.checkBoxOperator.AutoSize = true;
            this.checkBoxOperator.Location = new System.Drawing.Point(13, 152);
            this.checkBoxOperator.Name = "checkBoxOperator";
            this.checkBoxOperator.Size = new System.Drawing.Size(96, 21);
            this.checkBoxOperator.TabIndex = 33;
            this.checkBoxOperator.Text = "Оператор";
            this.checkBoxOperator.UseVisualStyleBackColor = true;
            this.checkBoxOperator.CheckedChanged += new System.EventHandler(this.buttonApply_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 100);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(26, 17);
            this.label6.TabIndex = 27;
            this.label6.Text = "По";
            // 
            // dateTimePickerTo
            // 
            this.dateTimePickerTo.Location = new System.Drawing.Point(61, 97);
            this.dateTimePickerTo.Name = "dateTimePickerTo";
            this.dateTimePickerTo.Size = new System.Drawing.Size(161, 22);
            this.dateTimePickerTo.TabIndex = 26;
            this.dateTimePickerTo.ValueChanged += new System.EventHandler(this.buttonApply_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 17);
            this.label1.TabIndex = 20;
            this.label1.Text = "С";
            // 
            // checkBoxTime
            // 
            this.checkBoxTime.AutoSize = true;
            this.checkBoxTime.Location = new System.Drawing.Point(13, 31);
            this.checkBoxTime.Name = "checkBoxTime";
            this.checkBoxTime.Size = new System.Drawing.Size(72, 21);
            this.checkBoxTime.TabIndex = 19;
            this.checkBoxTime.Text = "Время";
            this.checkBoxTime.UseVisualStyleBackColor = true;
            this.checkBoxTime.CheckedChanged += new System.EventHandler(this.buttonApply_Click);
            // 
            // проведенноеМероприятиеTableAdapter
            // 
            this.проведенноеМероприятиеTableAdapter.ClearBeforeFill = true;
            // 
            // проведенноеМероприятиеViewTableAdapter
            // 
            this.проведенноеМероприятиеViewTableAdapter.ClearBeforeFill = true;
            // 
            // пользовательTableAdapter
            // 
            this.пользовательTableAdapter.ClearBeforeFill = true;
            // 
            // buttonOK
            // 
            this.buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOK.Location = new System.Drawing.Point(796, 250);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(113, 29);
            this.buttonOK.TabIndex = 20;
            this.buttonOK.Text = "ОК";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // ActionHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(921, 291);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "ActionHistory";
            this.Text = "История";
            this.Load += new System.EventHandler(this.History_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.проведенноеМероприятиеViewBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.oilDBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.проведенноеМероприятиеBindingSource)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.пользовательBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DateTimePicker dateTimePickerFrom;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox comboBoxOperator;
        private System.Windows.Forms.CheckBox checkBoxOperator;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dateTimePickerTo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBoxTime;
        private OilDBDataSet oilDBDataSet;
        private System.Windows.Forms.BindingSource проведенноеМероприятиеBindingSource;
        private OilDBDataSetTableAdapters.ПроведенноеМероприятиеTableAdapter проведенноеМероприятиеTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn датаИВремяDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource проведенноеМероприятиеViewBindingSource;
        private OilDBDataSetTableAdapters.ПроведенноеМероприятиеViewTableAdapter проведенноеМероприятиеViewTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDФактаDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn логинОператораDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn информацияОПроведенииDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn названиеМероприятияDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn описаниеМероприятияDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource пользовательBindingSource;
        private OilDBDataSetTableAdapters.ПользовательTableAdapter пользовательTableAdapter;
        private System.Windows.Forms.Button buttonOK;
    }
}