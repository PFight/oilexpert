﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OilExpert
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void редактироватьДатчикиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (new SensorsView()).Show();
        }

        private void редактироватьФактыToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            (new Facts()).Show();
        }

        private void редактироватьПравилаToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            (new Rules()).Show();
        }

        private void сменитьПользователяToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            notifyIcon1.ShowBalloonTip(1500);
            
        }

        private void тестированиеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TestInputSensors wnd = new TestInputSensors();
            wnd.ShowDialog();
        }

        private void историяМероприятийToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (new ActionHistory()).Show();
        }

        private void историяToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (new MalfunctionHistory()).Show();
        }

        
    }
}
