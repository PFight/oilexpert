﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OilExpert
{
    public partial class AddRule : Form
    {
        public AddRule()
        {
            InitializeComponent();
        }

        private void AddRule_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'oilDBDataSet.Факт' table. You can move, or remove it, as needed.
            this.фактTableAdapter.Fill(this.oilDBDataSet.Факт);

        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
            Hide();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            Hide();
        }

        private void buttonAddToAntecendent_Click(object sender, EventArgs e)
        {
            if (dataGridViewFacts.CurrentRow != null)
            {
                int factID = (int)dataGridViewFacts.CurrentRow.Cells[0].Value;
                string name = Program.Convert<string>(dataGridViewFacts.CurrentRow.Cells[2].Value, "безымянный факт");
                string id = "f" + factID.ToString();
                dataGridViewAntecendent.Rows.Add(factID, name, id);
                string formula = textBoxFormulaOfAntecendent.Text;
                if (formula.Length > 0)
                {
                    formula = formula + " && ";
                }
                formula = formula + id;
                textBoxFormulaOfAntecendent.Text = formula;
            }
            else
            {
                MessageBox.Show("Выберите, пожалуйста, факт");
            }
        }

        private void buttonAddToConsecvent_Click(object sender, EventArgs e)
        {
            if (dataGridViewFacts.CurrentRow != null)
            {
                int factID = (int)dataGridViewFacts.CurrentRow.Cells[0].Value;
                string name = Program.Convert<string>(dataGridViewFacts.CurrentRow.Cells[2].Value, "безымянный факт");
                dataGridViewConsecvent.Rows.Add(factID, name);
            }
            else
            {
                MessageBox.Show("Выберите, пожалуйста, факт");
            }
        }

        private void buttonRemoveFromAntecendent_Click(object sender, EventArgs e)
        {
            if (dataGridViewAntecendent.CurrentRow != null)
            {
                dataGridViewAntecendent.Rows.Remove(dataGridViewAntecendent.CurrentRow as DataGridViewRow);
            }
            else
            {
                MessageBox.Show("Выберите, пожалуйста, факт антецендента");
            }
        }

        private void buttonRemoveFromConsecvent_Click(object sender, EventArgs e)
        {
            if (dataGridViewConsecvent.CurrentRow != null)
            {
                dataGridViewConsecvent.Rows.Remove(dataGridViewConsecvent.CurrentRow as DataGridViewRow);
            }
            else
            {
                MessageBox.Show("Выберите, пожалуйста, факт консеквента");
            }
        }
    }
}
