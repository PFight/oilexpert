﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OilExpert
{
    public partial class MalfunctionHistory : Form
    {
        public MalfunctionHistory()
        {
            InitializeComponent();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void MalfunctionHistory_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'oilDBDataSet1.Пользователь' table. You can move, or remove it, as needed.
            this.пользовательTableAdapter.Fill(this.oilDBDataSet.Пользователь);
            // TODO: This line of code loads data into the 'oilDBDataSet1.ЗафиксированныйСбойView' table. You can move, or remove it, as needed.
            this.зафиксированныйСбойViewTableAdapter.Fill(this.oilDBDataSet.ЗафиксированныйСбойView);

        }

        private void applyFilter(object sender, EventArgs e)
        {
            if (checkBoxOperator.Checked && checkBoxTime.Checked)
            {
                this.зафиксированныйСбойViewTableAdapter.FillByTimeAndOperator(
                    this.oilDBDataSet.ЗафиксированныйСбойView, dateTimePickerFrom.Value.AddDays(-1),
                    dateTimePickerTo.Value, comboBoxOperator.Text);
                зафиксированныйСбойViewBindingSource.ResetBindings(false);
                зафиксированныйСбойBindingSource.ResetBindings(false);
            }
            else if (checkBoxOperator.Checked && !checkBoxTime.Checked)
            {
                this.зафиксированныйСбойViewTableAdapter.FillByOperator(
                    this.oilDBDataSet.ЗафиксированныйСбойView, comboBoxOperator.Text);
                зафиксированныйСбойViewBindingSource.ResetBindings(false);
                зафиксированныйСбойBindingSource.ResetBindings(false);
            }
            else if (!checkBoxOperator.Checked && checkBoxTime.Checked)
            {
                this.зафиксированныйСбойViewTableAdapter.FillByTime(
                    this.oilDBDataSet.ЗафиксированныйСбойView, dateTimePickerFrom.Value.AddDays(-1),
                    dateTimePickerTo.Value);
                зафиксированныйСбойViewBindingSource.ResetBindings(false);
                зафиксированныйСбойBindingSource.ResetBindings(false);
            }
            else if (!checkBoxOperator.Checked && !checkBoxTime.Checked)
            {
                this.зафиксированныйСбойViewTableAdapter.Fill(
                    this.oilDBDataSet.ЗафиксированныйСбойView);
                зафиксированныйСбойViewBindingSource.ResetBindings(false);
                зафиксированныйСбойBindingSource.ResetBindings(false);
            }
        }

    }
}
