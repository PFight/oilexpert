﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OilExpert
{
    public partial class SensorsView : Form
    {
        public SensorsView()
        {
            InitializeComponent();
        }

        private void SensorsView_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'oilDBDataSet.Датчик' table. You can move, or remove it, as needed.
            this.датчикTableAdapter.Fill(this.oilDBDataSet.Датчик);

        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            var wnd = new AddSensor();
            wnd.Text = "Добавление датчика";
            wnd.buttonOk.Text = "Добавить";
            wnd.comboBoxType.SelectedIndex = 0;
            if (wnd.ShowDialog() == DialogResult.OK)
            {
                датчикTableAdapter.Insert(wnd.textBoxID.Text,
                    wnd.textBoxLocation.Text, wnd.comboBoxType.Text);
                датчикTableAdapter.Fill(oilDBDataSet.Датчик);
                датчикBindingSource.ResetBindings(false);
            }
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            string id = Program.Convert<string>(dataGridView1.CurrentRow.Cells[0].Value, "");
            string type = Program.Convert<string>(dataGridView1.CurrentRow.Cells[1].Value, "");
            string location = Program.Convert<string>(dataGridView1.CurrentRow.Cells[2].Value, "");
            var wnd = new AddSensor();
            wnd.textBoxID.Text = id;
            wnd.textBoxLocation.Text = location;
            wnd.comboBoxType.Text = type;
            if (wnd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                датчикTableAdapter.Update(wnd.textBoxID.Text,
                    wnd.textBoxLocation.Text, wnd.comboBoxType.Text, wnd.textBoxID.Text);
                датчикTableAdapter.Fill(oilDBDataSet.Датчик);
                датчикBindingSource.ResetBindings(false);
            }
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentRow != null)
            {
                if (MessageBox.Show(this, "Вы действительно хотите удалить датчик?",
                    "Подтверждение", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    string id = Program.Convert<string>(dataGridView1.CurrentRow.Cells[0].Value, "");
                    датчикTableAdapter.Delete(id);
                    датчикTableAdapter.Fill(oilDBDataSet.Датчик);
                    датчикBindingSource.ResetBindings(false);
                }
            }
            else
            {
                MessageBox.Show("Выберите, пожалуйста, датчик");
            }
        }

        private void buttonToFacts_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentRow != null)
            {
                string id = Program.Convert<string>(dataGridView1.CurrentRow.Cells[0].Value, "");
                (new ReadoutFact(id)).Show();
            }
            else
            {
                MessageBox.Show("Выберите, пожалуйста, датчик");
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
