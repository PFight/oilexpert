﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace OilExpert
{
    static class Program
    {
        public static string CurrentUser
        {
            get;
            private set;
        }

        public static MainForm MainForm
        {
            get;
            private set;
        }


        public static T Convert<T>(object value, T defaultValue)
        {
            if (value is DBNull || value == null)
            {
                return defaultValue;
            }
            else
            {
                return (T)value;
            }
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Login loginDialog = new Login();
            if (loginDialog.ShowDialog() == DialogResult.OK)
            {
                CurrentUser = loginDialog.textBoxLogin.Text;
                //Application.Run(new SensorsView());
                MainForm = new MainForm();
                Application.Run(MainForm);
            }
        }
    }
}
