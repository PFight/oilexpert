﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OilExpert
{
    public partial class TestInputSensors : Form
    {
        public TestInputSensors()
        {
            InitializeComponent();
        }

        private void TestInputSensors_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'oilDBDataSet.Датчик' table. You can move, or remove it, as needed.
            this.датчикTableAdapter.Fill(this.oilDBDataSet.Датчик);

        }

        private void buttonGo_Click(object sender, EventArgs e)
        {
            List<int> facts = new List<int>();
            foreach (var rowObj in dataGridView1.Rows)
            {
                DataGridViewRow row = rowObj as DataGridViewRow;
                string sensorID = row.Cells[0].Value as string;
                string readoutStr = row.Cells[3].Value as string;
                float readout;
                if (float.TryParse(readoutStr, out readout))
                {
                    int? factID = показаниеДатчикаФактTableAdapter.GetFactByReadout(sensorID, readout);
                    if (factID.HasValue)
                    {
                        facts.Add(factID.Value);
                    }
                }
                else
                {
                    if (readoutStr != null && readoutStr.Length > 0)
                    {
                        MessageBox.Show(readoutStr + " не может быть распознано как число");
                    }
                }
            }

            List<int> result = DeductionMachine.Run(facts);

            List<int> malfunctions = new List<int>();
            List<int> actions = new List<int>();
            foreach (int factID in result)
            {
                string factType = фактTableAdapter1.GetFactType(factID);
                if (factType == "Мероприятие")
                {
                    actions.Add(factID);
                }
                else if (factType == "Сбой")
                {
                    malfunctions.Add(factID);
                }
            }

            if (actions.Count > 0)
            {
                WarnWnd wnd = new WarnWnd();
                wnd.ShowDeductionResult(actions);
            }
            if (malfunctions.Count > 0)
            {
                foreach (int factID in malfunctions)
                {
                    зафиксированныйСбойTableAdapter1.Insert(DateTime.Now, factID, Program.CurrentUser);
                }
                Program.MainForm.notifyIcon1.ShowBalloonTip(5000, "OilExpert - обнаружен сбой", 
                    "Зафикисрован сбой в работе системы." +
                    " За подробностями, обратитесь к журналу сбоев.", 
                    ToolTipIcon.Warning); 
            }
        }
    }
}
