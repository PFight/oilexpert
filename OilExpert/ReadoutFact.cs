﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OilExpert
{
    public partial class ReadoutFact : Form
    {
        string mSensorID;

        public ReadoutFact(string sensorID)
        {
            mSensorID = sensorID;
            InitializeComponent();
            labelSensorID.Text = sensorID;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void ReadoutFact_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'oilDBDataSet.ПоказаниеДатчикаФактView' table. You can move, or remove it, as needed.
            this.показаниеДатчикаФактViewTableAdapter.FillBy(this.oilDBDataSet.ПоказаниеДатчикаФактView, mSensorID);
            // TODO: This line of code loads data into the 'oilDBDataSet.ПоказаниеДатчикаФакт' table. You can move, or remove it, as needed.
            this.показаниеДатчикаФактTableAdapter.Fill(this.oilDBDataSet.ПоказаниеДатчикаФакт);

        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            var wnd = new AddReadoutFact();
            wnd.Text = "Добавление сопоставления";
            wnd.buttonOk.Text = "Добавить";
            wnd.labelSensorID.Text = mSensorID;
            if (wnd.ShowDialog() == DialogResult.OK)
            {
                int factID = Program.Convert<int>(wnd.dataGridViewFacts.CurrentRow.Cells[0].Value, 0);
                показаниеДатчикаФактTableAdapter.Insert((float)wnd.numericUpDownLower.Value,
                    (float)wnd.numericUpDownUpper.Value, factID, mSensorID);
                показаниеДатчикаФактViewTableAdapter.FillBy(oilDBDataSet.ПоказаниеДатчикаФактView, mSensorID);
                показаниеДатчикаФактBindingSource.ResetBindings(false);
            }
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentRow != null)
            {
                string sensorID = Program.Convert<string>(dataGridView1.CurrentRow.Cells[0].Value, "");
                float from = Program.Convert<float>(dataGridView1.CurrentRow.Cells[1].Value, 0);
                float to = Program.Convert<float>(dataGridView1.CurrentRow.Cells[2].Value, 0);
                int factID = Program.Convert<int>(dataGridView1.CurrentRow.Cells[3].Value, 0);
                var wnd = new AddReadoutFact();
                wnd.numericUpDownLower.Value = (decimal)from;
                wnd.numericUpDownUpper.Value = (decimal)to;
                wnd.mFactToSelectAfterShow = factID;

                if (wnd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    factID = Program.Convert<int>(wnd.dataGridViewFacts.CurrentRow.Cells[0].Value, 0);
                    показаниеДатчикаФактTableAdapter.Delete(from, to, mSensorID);
                    показаниеДатчикаФактTableAdapter.Insert((float)wnd.numericUpDownLower.Value,
                        (float)wnd.numericUpDownUpper.Value, factID, mSensorID);
                    показаниеДатчикаФактViewTableAdapter.FillBy(oilDBDataSet.ПоказаниеДатчикаФактView, mSensorID);
                    показаниеДатчикаФактBindingSource.ResetBindings(false);
                }
            }
            else
            {
                MessageBox.Show("Выберите, пожалуйста, сопоставление");
            }
        }

        private void buttonОК_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentRow != null)
            {
                if (MessageBox.Show(this, "Вы действительно хотите удалить сопоставление?",
                    "Подтверждение", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    string sensorID = Program.Convert<string>(dataGridView1.CurrentRow.Cells[0].Value, "");
                    float from = Program.Convert<float>(dataGridView1.CurrentRow.Cells[1].Value, 0);
                    float to = Program.Convert<float>(dataGridView1.CurrentRow.Cells[2].Value, 0);
                    показаниеДатчикаФактTableAdapter.Delete(from, to, sensorID);
                    показаниеДатчикаФактViewTableAdapter.FillBy(oilDBDataSet.ПоказаниеДатчикаФактView, mSensorID);
                    показаниеДатчикаФактBindingSource.ResetBindings(false);
                }
            }
            else
            {
                MessageBox.Show("Выберите, пожалуйста, сопоставление");
            }
        }
    }
}
