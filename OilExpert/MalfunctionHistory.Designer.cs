﻿namespace OilExpert
{
    partial class MalfunctionHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonOK = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboBoxOperator = new System.Windows.Forms.ComboBox();
            this.checkBoxOperator = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dateTimePickerTo = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBoxTime = new System.Windows.Forms.CheckBox();
            this.dateTimePickerFrom = new System.Windows.Forms.DateTimePicker();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.датаИВремяDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDФактаDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.логинОператораDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.названиеDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.описаниеDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.зафиксированныйСбойViewBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.oilDBDataSet = new OilExpert.OilDBDataSet();
            this.зафиксированныйСбойBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.зафиксированныйСбойTableAdapter = new OilExpert.OilDBDataSetTableAdapters.ЗафиксированныйСбойTableAdapter();
            this.зафиксированныйСбойViewTableAdapter = new OilExpert.OilDBDataSetTableAdapters.ЗафиксированныйСбойViewTableAdapter();
            this.пользовательBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.пользовательTableAdapter = new OilExpert.OilDBDataSetTableAdapters.ПользовательTableAdapter();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.зафиксированныйСбойViewBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.oilDBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.зафиксированныйСбойBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.пользовательBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonOK
            // 
            this.buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOK.Location = new System.Drawing.Point(806, 271);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(113, 29);
            this.buttonOK.TabIndex = 23;
            this.buttonOK.Text = "ОК";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.comboBoxOperator);
            this.groupBox1.Controls.Add(this.checkBoxOperator);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.dateTimePickerTo);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.checkBoxTime);
            this.groupBox1.Controls.Add(this.dateTimePickerFrom);
            this.groupBox1.Location = new System.Drawing.Point(673, 16);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(246, 238);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Фильтр";
            // 
            // comboBoxOperator
            // 
            this.comboBoxOperator.DataSource = this.пользовательBindingSource;
            this.comboBoxOperator.DisplayMember = "Логин";
            this.comboBoxOperator.FormattingEnabled = true;
            this.comboBoxOperator.Location = new System.Drawing.Point(22, 189);
            this.comboBoxOperator.Name = "comboBoxOperator";
            this.comboBoxOperator.Size = new System.Drawing.Size(212, 24);
            this.comboBoxOperator.TabIndex = 34;
            this.comboBoxOperator.ValueMember = "Логин";
            this.comboBoxOperator.SelectedValueChanged += new System.EventHandler(this.applyFilter);
            // 
            // checkBoxOperator
            // 
            this.checkBoxOperator.AutoSize = true;
            this.checkBoxOperator.Location = new System.Drawing.Point(13, 152);
            this.checkBoxOperator.Name = "checkBoxOperator";
            this.checkBoxOperator.Size = new System.Drawing.Size(96, 21);
            this.checkBoxOperator.TabIndex = 33;
            this.checkBoxOperator.Text = "Оператор";
            this.checkBoxOperator.UseVisualStyleBackColor = true;
            this.checkBoxOperator.CheckedChanged += new System.EventHandler(this.applyFilter);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 100);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(26, 17);
            this.label6.TabIndex = 27;
            this.label6.Text = "По";
            // 
            // dateTimePickerTo
            // 
            this.dateTimePickerTo.Location = new System.Drawing.Point(61, 97);
            this.dateTimePickerTo.Name = "dateTimePickerTo";
            this.dateTimePickerTo.Size = new System.Drawing.Size(161, 22);
            this.dateTimePickerTo.TabIndex = 26;
            this.dateTimePickerTo.ValueChanged += new System.EventHandler(this.applyFilter);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 17);
            this.label1.TabIndex = 20;
            this.label1.Text = "С";
            // 
            // checkBoxTime
            // 
            this.checkBoxTime.AutoSize = true;
            this.checkBoxTime.Location = new System.Drawing.Point(13, 31);
            this.checkBoxTime.Name = "checkBoxTime";
            this.checkBoxTime.Size = new System.Drawing.Size(72, 21);
            this.checkBoxTime.TabIndex = 19;
            this.checkBoxTime.Text = "Время";
            this.checkBoxTime.UseVisualStyleBackColor = true;
            this.checkBoxTime.CheckedChanged += new System.EventHandler(this.applyFilter);
            // 
            // dateTimePickerFrom
            // 
            this.dateTimePickerFrom.Location = new System.Drawing.Point(61, 69);
            this.dateTimePickerFrom.Name = "dateTimePickerFrom";
            this.dateTimePickerFrom.Size = new System.Drawing.Size(161, 22);
            this.dateTimePickerFrom.TabIndex = 18;
            this.dateTimePickerFrom.ValueChanged += new System.EventHandler(this.applyFilter);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.датаИВремяDataGridViewTextBoxColumn,
            this.iDФактаDataGridViewTextBoxColumn,
            this.логинОператораDataGridViewTextBoxColumn,
            this.названиеDataGridViewTextBoxColumn,
            this.описаниеDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.зафиксированныйСбойViewBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(10, 14);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(658, 286);
            this.dataGridView1.TabIndex = 21;
            // 
            // датаИВремяDataGridViewTextBoxColumn
            // 
            this.датаИВремяDataGridViewTextBoxColumn.DataPropertyName = "ДатаИВремя";
            this.датаИВремяDataGridViewTextBoxColumn.FillWeight = 50F;
            this.датаИВремяDataGridViewTextBoxColumn.HeaderText = "Время";
            this.датаИВремяDataGridViewTextBoxColumn.Name = "датаИВремяDataGridViewTextBoxColumn";
            this.датаИВремяDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // iDФактаDataGridViewTextBoxColumn
            // 
            this.iDФактаDataGridViewTextBoxColumn.DataPropertyName = "IDФакта";
            this.iDФактаDataGridViewTextBoxColumn.HeaderText = "IDФакта";
            this.iDФактаDataGridViewTextBoxColumn.Name = "iDФактаDataGridViewTextBoxColumn";
            this.iDФактаDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDФактаDataGridViewTextBoxColumn.Visible = false;
            // 
            // логинОператораDataGridViewTextBoxColumn
            // 
            this.логинОператораDataGridViewTextBoxColumn.DataPropertyName = "ЛогинОператора";
            this.логинОператораDataGridViewTextBoxColumn.FillWeight = 50F;
            this.логинОператораDataGridViewTextBoxColumn.HeaderText = "Оператор";
            this.логинОператораDataGridViewTextBoxColumn.Name = "логинОператораDataGridViewTextBoxColumn";
            this.логинОператораDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // названиеDataGridViewTextBoxColumn
            // 
            this.названиеDataGridViewTextBoxColumn.DataPropertyName = "Название";
            this.названиеDataGridViewTextBoxColumn.HeaderText = "Сбой";
            this.названиеDataGridViewTextBoxColumn.Name = "названиеDataGridViewTextBoxColumn";
            this.названиеDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // описаниеDataGridViewTextBoxColumn
            // 
            this.описаниеDataGridViewTextBoxColumn.DataPropertyName = "Описание";
            this.описаниеDataGridViewTextBoxColumn.FillWeight = 50F;
            this.описаниеDataGridViewTextBoxColumn.HeaderText = "Описание";
            this.описаниеDataGridViewTextBoxColumn.Name = "описаниеDataGridViewTextBoxColumn";
            this.описаниеDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // зафиксированныйСбойViewBindingSource
            // 
            this.зафиксированныйСбойViewBindingSource.DataMember = "ЗафиксированныйСбойView";
            this.зафиксированныйСбойViewBindingSource.DataSource = this.oilDBDataSet;
            // 
            // oilDBDataSet
            // 
            this.oilDBDataSet.DataSetName = "OilDBDataSet";
            this.oilDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // зафиксированныйСбойBindingSource
            // 
            this.зафиксированныйСбойBindingSource.DataMember = "ЗафиксированныйСбой";
            this.зафиксированныйСбойBindingSource.DataSource = this.oilDBDataSet;
            // 
            // зафиксированныйСбойTableAdapter
            // 
            this.зафиксированныйСбойTableAdapter.ClearBeforeFill = true;
            // 
            // зафиксированныйСбойViewTableAdapter
            // 
            this.зафиксированныйСбойViewTableAdapter.ClearBeforeFill = true;
            // 
            // пользовательBindingSource
            // 
            this.пользовательBindingSource.DataMember = "Пользователь";
            this.пользовательBindingSource.DataSource = this.oilDBDataSet;
            // 
            // пользовательTableAdapter
            // 
            this.пользовательTableAdapter.ClearBeforeFill = true;
            // 
            // MalfunctionHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(929, 312);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "MalfunctionHistory";
            this.Text = "MalfunctionHistory";
            this.Load += new System.EventHandler(this.MalfunctionHistory_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.зафиксированныйСбойViewBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.oilDBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.зафиксированныйСбойBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.пользовательBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox comboBoxOperator;
        private System.Windows.Forms.CheckBox checkBoxOperator;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dateTimePickerTo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBoxTime;
        private System.Windows.Forms.DateTimePicker dateTimePickerFrom;
        private System.Windows.Forms.DataGridView dataGridView1;
        private OilDBDataSet oilDBDataSet;
        private System.Windows.Forms.BindingSource зафиксированныйСбойBindingSource;
        private OilDBDataSetTableAdapters.ЗафиксированныйСбойTableAdapter зафиксированныйСбойTableAdapter;
        private System.Windows.Forms.BindingSource зафиксированныйСбойViewBindingSource;
        private OilDBDataSetTableAdapters.ЗафиксированныйСбойViewTableAdapter зафиксированныйСбойViewTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn датаИВремяDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDФактаDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn логинОператораDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn названиеDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn описаниеDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource пользовательBindingSource;
        private OilDBDataSetTableAdapters.ПользовательTableAdapter пользовательTableAdapter;

    }
}