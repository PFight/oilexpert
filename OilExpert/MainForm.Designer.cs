﻿namespace OilExpert
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.сменитьПользователяToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.историяToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.историяМероприятийToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.прибритениеЗнанийToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.редактироватьДатчикиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.редактироватьФактыToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.редактироватьПравилаToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.тестированиеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipText = "Систе́ма подде́ржки приня́тия реше́ний для нефтестанции";
            this.notifyIcon1.BalloonTipTitle = "OilExpert";
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "OilExpert";
            this.notifyIcon1.Visible = true;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.сменитьПользователяToolStripMenuItem,
            this.историяToolStripMenuItem,
            this.историяМероприятийToolStripMenuItem,
            this.прибритениеЗнанийToolStripMenuItem,
            this.выходToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(239, 124);
            // 
            // сменитьПользователяToolStripMenuItem
            // 
            this.сменитьПользователяToolStripMenuItem.Name = "сменитьПользователяToolStripMenuItem";
            this.сменитьПользователяToolStripMenuItem.Size = new System.Drawing.Size(238, 24);
            this.сменитьПользователяToolStripMenuItem.Text = "Сменить пользователя";
            this.сменитьПользователяToolStripMenuItem.Click += new System.EventHandler(this.сменитьПользователяToolStripMenuItem_Click);
            // 
            // историяToolStripMenuItem
            // 
            this.историяToolStripMenuItem.Name = "историяToolStripMenuItem";
            this.историяToolStripMenuItem.Size = new System.Drawing.Size(238, 24);
            this.историяToolStripMenuItem.Text = "Журнал сбоев";
            this.историяToolStripMenuItem.Click += new System.EventHandler(this.историяToolStripMenuItem_Click);
            // 
            // историяМероприятийToolStripMenuItem
            // 
            this.историяМероприятийToolStripMenuItem.Name = "историяМероприятийToolStripMenuItem";
            this.историяМероприятийToolStripMenuItem.Size = new System.Drawing.Size(238, 24);
            this.историяМероприятийToolStripMenuItem.Text = "История мероприятий";
            this.историяМероприятийToolStripMenuItem.Click += new System.EventHandler(this.историяМероприятийToolStripMenuItem_Click);
            // 
            // прибритениеЗнанийToolStripMenuItem
            // 
            this.прибритениеЗнанийToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.редактироватьДатчикиToolStripMenuItem,
            this.редактироватьФактыToolStripMenuItem1,
            this.редактироватьПравилаToolStripMenuItem1,
            this.тестированиеToolStripMenuItem});
            this.прибритениеЗнанийToolStripMenuItem.Name = "прибритениеЗнанийToolStripMenuItem";
            this.прибритениеЗнанийToolStripMenuItem.Size = new System.Drawing.Size(238, 24);
            this.прибритениеЗнанийToolStripMenuItem.Text = "Прибритение знаний";
            // 
            // редактироватьДатчикиToolStripMenuItem
            // 
            this.редактироватьДатчикиToolStripMenuItem.Name = "редактироватьДатчикиToolStripMenuItem";
            this.редактироватьДатчикиToolStripMenuItem.Size = new System.Drawing.Size(243, 24);
            this.редактироватьДатчикиToolStripMenuItem.Text = "Редактировать датчики";
            this.редактироватьДатчикиToolStripMenuItem.Click += new System.EventHandler(this.редактироватьДатчикиToolStripMenuItem_Click);
            // 
            // редактироватьФактыToolStripMenuItem1
            // 
            this.редактироватьФактыToolStripMenuItem1.Name = "редактироватьФактыToolStripMenuItem1";
            this.редактироватьФактыToolStripMenuItem1.Size = new System.Drawing.Size(243, 24);
            this.редактироватьФактыToolStripMenuItem1.Text = "Редактировать факты";
            this.редактироватьФактыToolStripMenuItem1.Click += new System.EventHandler(this.редактироватьФактыToolStripMenuItem1_Click);
            // 
            // редактироватьПравилаToolStripMenuItem1
            // 
            this.редактироватьПравилаToolStripMenuItem1.Name = "редактироватьПравилаToolStripMenuItem1";
            this.редактироватьПравилаToolStripMenuItem1.Size = new System.Drawing.Size(243, 24);
            this.редактироватьПравилаToolStripMenuItem1.Text = "Редактировать правила";
            this.редактироватьПравилаToolStripMenuItem1.Click += new System.EventHandler(this.редактироватьПравилаToolStripMenuItem1_Click);
            // 
            // тестированиеToolStripMenuItem
            // 
            this.тестированиеToolStripMenuItem.Name = "тестированиеToolStripMenuItem";
            this.тестированиеToolStripMenuItem.Size = new System.Drawing.Size(243, 24);
            this.тестированиеToolStripMenuItem.Text = "Тестирование системы";
            this.тестированиеToolStripMenuItem.Click += new System.EventHandler(this.тестированиеToolStripMenuItem_Click);
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(238, 24);
            this.выходToolStripMenuItem.Text = "Выход";
            this.выходToolStripMenuItem.Click += new System.EventHandler(this.выходToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(282, 255);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.ShowInTaskbar = false;
            this.Text = "OilExpert";
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem сменитьПользователяToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem историяToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem историяМероприятийToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem прибритениеЗнанийToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem редактироватьДатчикиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem редактироватьФактыToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem редактироватьПравилаToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem тестированиеToolStripMenuItem;
        public System.Windows.Forms.NotifyIcon notifyIcon1;
    }
}