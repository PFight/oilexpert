﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OilExpert
{
    public partial class ActionHistory : Form
    {
        public ActionHistory()
        {
            InitializeComponent();
        }

        private void History_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'oilDBDataSet.Пользователь' table. You can move, or remove it, as needed.
            this.пользовательTableAdapter.Fill(this.oilDBDataSet.Пользователь);
            // TODO: This line of code loads data into the 'oilDBDataSet.ПроведенноеМероприятиеView' table. You can move, or remove it, as needed.
            this.проведенноеМероприятиеViewTableAdapter.Fill(this.oilDBDataSet.ПроведенноеМероприятиеView);

        }

        private void buttonApply_Click(object sender, EventArgs e)
        {
            if (checkBoxOperator.Checked && checkBoxTime.Checked)
            {
                this.проведенноеМероприятиеViewTableAdapter.FillByTimeAndOperator(
                    this.oilDBDataSet.ПроведенноеМероприятиеView, dateTimePickerFrom.Value.AddDays(-1), 
                    dateTimePickerTo.Value, comboBoxOperator.Text);
                проведенноеМероприятиеBindingSource.ResetBindings(false);
            }
            else if (checkBoxOperator.Checked && !checkBoxTime.Checked)
            {
                this.проведенноеМероприятиеViewTableAdapter.FillByOperator(
                    this.oilDBDataSet.ПроведенноеМероприятиеView, comboBoxOperator.Text);
                проведенноеМероприятиеBindingSource.ResetBindings(false);
            }
            else if (!checkBoxOperator.Checked && checkBoxTime.Checked)
            {
                this.проведенноеМероприятиеViewTableAdapter.FillByTime(
                    this.oilDBDataSet.ПроведенноеМероприятиеView, dateTimePickerFrom.Value.AddDays(-1),
                    dateTimePickerTo.Value);
                проведенноеМероприятиеBindingSource.ResetBindings(false);
            }
            else if (!checkBoxOperator.Checked && !checkBoxTime.Checked)
            {
                this.проведенноеМероприятиеViewTableAdapter.Fill(
                    this.oilDBDataSet.ПроведенноеМероприятиеView);
                проведенноеМероприятиеBindingSource.ResetBindings(false);
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
