﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OilExpert.OilDBDataSetTableAdapters;

namespace OilExpert
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool found = false;
            bool pass = false;
            foreach (OilDBDataSet.ПользовательRow row in (new ПользовательTableAdapter()).GetData())
            {
                if (row.Логин == textBoxLogin.Text)
                {
                    found = true;
                    // TODO: encrypt pass
                    if (row.Пароль == maskedTextBoxPass.Text)
                    {
                        pass = true;
                    }
                }
            }

            if (pass)
            {
                DialogResult = System.Windows.Forms.DialogResult.OK;
                Close();
            }
            else
            {
                if (found)
                {
                    MessageBox.Show("Неверный пароль (проверьте состояние CapsLock и раскладку)");
                }
                else
                {
                    MessageBox.Show("Неверный логин (проверьте состояние CapsLock и раскладку)");
                }
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }
    }
}
