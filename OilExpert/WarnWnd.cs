﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OilExpert
{
    public partial class WarnWnd : Form
    {
        public WarnWnd()
        {
            InitializeComponent();
        }

        public void ShowDeductionResult(List<int> facts)
        {
            oilDBDataSet.Факт.Clear();

            foreach (int fact in facts)
            {
                фактTableAdapter1.FillBy(oilDBDataSet.Факт, fact);
            }

            if (!Visible)
            {
                Show();
            }
        }

        private void Warn_Load(object sender, EventArgs e)
        {

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            (new LogWnd()).Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonSelect_Click(object sender, EventArgs e)
        {
            foreach (var rowObj in dataGridView1.SelectedRows)
            {
                DataGridViewRow row = rowObj as DataGridViewRow;
                int affected = проведенноеМероприятиеTableAdapter.Insert(DateTime.Now,
                    (int)row.Cells[0].Value, Program.CurrentUser, "");
                Close();
            }
        }

    }
}
