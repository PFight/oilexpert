﻿namespace OilExpert
{
    partial class AddRule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridViewFacts = new System.Windows.Forms.DataGridView();
            this.фактBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.oilDBDataSet = new OilExpert.OilDBDataSet();
            this.фактTableAdapter = new OilExpert.OilDBDataSetTableAdapters.ФактTableAdapter();
            this.dataGridViewAntecendent = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxFormulaOfAntecendent = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonAddToAntecendent = new System.Windows.Forms.Button();
            this.buttonAddToConsecvent = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonRemoveFromAntecendent = new System.Windows.Forms.Button();
            this.dataGridViewConsecvent = new System.Windows.Forms.DataGridView();
            this.numericUpDownPriority = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonRemoveFromConsecvent = new System.Windows.Forms.Button();
            this.iDФактаDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.типDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.названиеDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.описаниеDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IDФакта = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fact = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IDInFormula = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FactID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFacts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.фактBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.oilDBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAntecendent)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewConsecvent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPriority)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewFacts
            // 
            this.dataGridViewFacts.AllowUserToAddRows = false;
            this.dataGridViewFacts.AllowUserToDeleteRows = false;
            this.dataGridViewFacts.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewFacts.AutoGenerateColumns = false;
            this.dataGridViewFacts.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewFacts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewFacts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDФактаDataGridViewTextBoxColumn,
            this.типDataGridViewTextBoxColumn,
            this.названиеDataGridViewTextBoxColumn,
            this.описаниеDataGridViewTextBoxColumn});
            this.dataGridViewFacts.DataSource = this.фактBindingSource;
            this.dataGridViewFacts.Location = new System.Drawing.Point(11, 259);
            this.dataGridViewFacts.Name = "dataGridViewFacts";
            this.dataGridViewFacts.ReadOnly = true;
            this.dataGridViewFacts.RowTemplate.Height = 24;
            this.dataGridViewFacts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewFacts.Size = new System.Drawing.Size(798, 190);
            this.dataGridViewFacts.TabIndex = 16;
            // 
            // фактBindingSource
            // 
            this.фактBindingSource.DataMember = "Факт";
            this.фактBindingSource.DataSource = this.oilDBDataSet;
            // 
            // oilDBDataSet
            // 
            this.oilDBDataSet.DataSetName = "OilDBDataSet";
            this.oilDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // фактTableAdapter
            // 
            this.фактTableAdapter.ClearBeforeFill = true;
            // 
            // dataGridViewAntecendent
            // 
            this.dataGridViewAntecendent.AllowUserToAddRows = false;
            this.dataGridViewAntecendent.AllowUserToDeleteRows = false;
            this.dataGridViewAntecendent.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewAntecendent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewAntecendent.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IDФакта,
            this.Fact,
            this.IDInFormula});
            this.dataGridViewAntecendent.Location = new System.Drawing.Point(6, 69);
            this.dataGridViewAntecendent.Name = "dataGridViewAntecendent";
            this.dataGridViewAntecendent.RowTemplate.Height = 24;
            this.dataGridViewAntecendent.Size = new System.Drawing.Size(383, 150);
            this.dataGridViewAntecendent.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(113, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 17);
            this.label1.TabIndex = 18;
            this.label1.Text = "Атецендент";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 17);
            this.label2.TabIndex = 19;
            this.label2.Text = "Формула:";
            // 
            // textBoxFormulaOfAntecendent
            // 
            this.textBoxFormulaOfAntecendent.Location = new System.Drawing.Point(82, 41);
            this.textBoxFormulaOfAntecendent.Name = "textBoxFormulaOfAntecendent";
            this.textBoxFormulaOfAntecendent.Size = new System.Drawing.Size(307, 22);
            this.textBoxFormulaOfAntecendent.TabIndex = 20;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(564, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 17);
            this.label3.TabIndex = 22;
            this.label3.Text = "Консеквент";
            // 
            // buttonAddToAntecendent
            // 
            this.buttonAddToAntecendent.BackgroundImage = global::OilExpert.Properties.Resources._1402349600_arrow_up;
            this.buttonAddToAntecendent.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonAddToAntecendent.Location = new System.Drawing.Point(65, 225);
            this.buttonAddToAntecendent.Name = "buttonAddToAntecendent";
            this.buttonAddToAntecendent.Size = new System.Drawing.Size(114, 28);
            this.buttonAddToAntecendent.TabIndex = 23;
            this.buttonAddToAntecendent.UseVisualStyleBackColor = true;
            this.buttonAddToAntecendent.Click += new System.EventHandler(this.buttonAddToAntecendent_Click);
            // 
            // buttonAddToConsecvent
            // 
            this.buttonAddToConsecvent.BackgroundImage = global::OilExpert.Properties.Resources._1402349600_arrow_up;
            this.buttonAddToConsecvent.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonAddToConsecvent.Location = new System.Drawing.Point(483, 225);
            this.buttonAddToConsecvent.Name = "buttonAddToConsecvent";
            this.buttonAddToConsecvent.Size = new System.Drawing.Size(114, 28);
            this.buttonAddToConsecvent.TabIndex = 24;
            this.buttonAddToConsecvent.UseVisualStyleBackColor = true;
            this.buttonAddToConsecvent.Click += new System.EventHandler(this.buttonAddToConsecvent_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.buttonRemoveFromConsecvent);
            this.groupBox1.Controls.Add(this.buttonRemoveFromAntecendent);
            this.groupBox1.Controls.Add(this.dataGridViewConsecvent);
            this.groupBox1.Controls.Add(this.buttonAddToConsecvent);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBoxFormulaOfAntecendent);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.buttonAddToAntecendent);
            this.groupBox1.Controls.Add(this.dataGridViewFacts);
            this.groupBox1.Controls.Add(this.dataGridViewAntecendent);
            this.groupBox1.Location = new System.Drawing.Point(12, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(823, 464);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            // 
            // buttonRemoveFromAntecendent
            // 
            this.buttonRemoveFromAntecendent.BackgroundImage = global::OilExpert.Properties.Resources._1402349603_arrow_down;
            this.buttonRemoveFromAntecendent.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonRemoveFromAntecendent.Location = new System.Drawing.Point(185, 225);
            this.buttonRemoveFromAntecendent.Name = "buttonRemoveFromAntecendent";
            this.buttonRemoveFromAntecendent.Size = new System.Drawing.Size(114, 28);
            this.buttonRemoveFromAntecendent.TabIndex = 26;
            this.buttonRemoveFromAntecendent.UseVisualStyleBackColor = true;
            this.buttonRemoveFromAntecendent.Click += new System.EventHandler(this.buttonRemoveFromAntecendent_Click);
            // 
            // dataGridViewConsecvent
            // 
            this.dataGridViewConsecvent.AllowUserToAddRows = false;
            this.dataGridViewConsecvent.AllowUserToDeleteRows = false;
            this.dataGridViewConsecvent.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewConsecvent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewConsecvent.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FactID,
            this.dataGridViewTextBoxColumn1});
            this.dataGridViewConsecvent.Location = new System.Drawing.Point(406, 41);
            this.dataGridViewConsecvent.Name = "dataGridViewConsecvent";
            this.dataGridViewConsecvent.RowTemplate.Height = 24;
            this.dataGridViewConsecvent.Size = new System.Drawing.Size(403, 178);
            this.dataGridViewConsecvent.TabIndex = 25;
            // 
            // numericUpDownPriority
            // 
            this.numericUpDownPriority.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.numericUpDownPriority.Location = new System.Drawing.Point(169, 472);
            this.numericUpDownPriority.Name = "numericUpDownPriority";
            this.numericUpDownPriority.Size = new System.Drawing.Size(120, 22);
            this.numericUpDownPriority.TabIndex = 27;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 474);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(139, 17);
            this.label4.TabIndex = 28;
            this.label4.Text = "Приоритет правила";
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Location = new System.Drawing.Point(468, 499);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(186, 32);
            this.buttonOk.TabIndex = 25;
            this.buttonOk.Text = "Добавить";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(660, 499);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(164, 32);
            this.buttonCancel.TabIndex = 29;
            this.buttonCancel.Text = "Отмена";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonRemoveFromConsecvent
            // 
            this.buttonRemoveFromConsecvent.BackgroundImage = global::OilExpert.Properties.Resources._1402349603_arrow_down;
            this.buttonRemoveFromConsecvent.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonRemoveFromConsecvent.Location = new System.Drawing.Point(603, 225);
            this.buttonRemoveFromConsecvent.Name = "buttonRemoveFromConsecvent";
            this.buttonRemoveFromConsecvent.Size = new System.Drawing.Size(114, 28);
            this.buttonRemoveFromConsecvent.TabIndex = 27;
            this.buttonRemoveFromConsecvent.UseVisualStyleBackColor = true;
            this.buttonRemoveFromConsecvent.Click += new System.EventHandler(this.buttonRemoveFromConsecvent_Click);
            // 
            // iDФактаDataGridViewTextBoxColumn
            // 
            this.iDФактаDataGridViewTextBoxColumn.DataPropertyName = "IDФакта";
            this.iDФактаDataGridViewTextBoxColumn.HeaderText = "IDФакта";
            this.iDФактаDataGridViewTextBoxColumn.Name = "iDФактаDataGridViewTextBoxColumn";
            this.iDФактаDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDФактаDataGridViewTextBoxColumn.Visible = false;
            // 
            // типDataGridViewTextBoxColumn
            // 
            this.типDataGridViewTextBoxColumn.DataPropertyName = "Тип";
            this.типDataGridViewTextBoxColumn.FillWeight = 50F;
            this.типDataGridViewTextBoxColumn.HeaderText = "Тип";
            this.типDataGridViewTextBoxColumn.Name = "типDataGridViewTextBoxColumn";
            this.типDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // названиеDataGridViewTextBoxColumn
            // 
            this.названиеDataGridViewTextBoxColumn.DataPropertyName = "Название";
            this.названиеDataGridViewTextBoxColumn.FillWeight = 200F;
            this.названиеDataGridViewTextBoxColumn.HeaderText = "Название факта";
            this.названиеDataGridViewTextBoxColumn.Name = "названиеDataGridViewTextBoxColumn";
            this.названиеDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // описаниеDataGridViewTextBoxColumn
            // 
            this.описаниеDataGridViewTextBoxColumn.DataPropertyName = "Описание";
            this.описаниеDataGridViewTextBoxColumn.HeaderText = "Описание";
            this.описаниеDataGridViewTextBoxColumn.Name = "описаниеDataGridViewTextBoxColumn";
            this.описаниеDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // IDФакта
            // 
            this.IDФакта.HeaderText = "IDФакта";
            this.IDФакта.Name = "IDФакта";
            this.IDФакта.Visible = false;
            // 
            // Fact
            // 
            this.Fact.HeaderText = "Факт";
            this.Fact.Name = "Fact";
            this.Fact.ReadOnly = true;
            // 
            // IDInFormula
            // 
            this.IDInFormula.FillWeight = 30F;
            this.IDInFormula.HeaderText = "ID в формуле";
            this.IDInFormula.Name = "IDInFormula";
            // 
            // FactID
            // 
            this.FactID.HeaderText = "FactID";
            this.FactID.Name = "FactID";
            this.FactID.Visible = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Факт";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // AddRule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(843, 543);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.numericUpDownPriority);
            this.Name = "AddRule";
            this.Text = "Добавление правила";
            this.Load += new System.EventHandler(this.AddRule_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFacts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.фактBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.oilDBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAntecendent)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewConsecvent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPriority)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.DataGridView dataGridViewFacts;
        private OilDBDataSet oilDBDataSet;
        private System.Windows.Forms.BindingSource фактBindingSource;
        private OilDBDataSetTableAdapters.ФактTableAdapter фактTableAdapter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonAddToAntecendent;
        private System.Windows.Forms.Button buttonAddToConsecvent;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonRemoveFromAntecendent;
        private System.Windows.Forms.Button buttonRemoveFromConsecvent;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDФактаDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn типDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn названиеDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn описаниеDataGridViewTextBoxColumn;
        public System.Windows.Forms.DataGridView dataGridViewAntecendent;
        public System.Windows.Forms.TextBox textBoxFormulaOfAntecendent;
        public System.Windows.Forms.DataGridView dataGridViewConsecvent;
        public System.Windows.Forms.NumericUpDown numericUpDownPriority;
        public System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDФакта;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fact;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDInFormula;
        private System.Windows.Forms.DataGridViewTextBoxColumn FactID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
    }
}