﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OilExpert.OilDBDataSetTableAdapters;
using NCalc;
using System.Windows.Forms;

namespace OilExpert
{
    static class DeductionMachine
    {
        class Rule
        {
            public int mID;
            public string mAntecendentFormula;
            public int mPriority;
            public Expression mAntecendentExpression;
            public Dictionary<int, string> mAntecendentFacts = new Dictionary<int,string>();
            public List<int> mConsecventFacts = new List<int>();
        }

        static ПравилоTableAdapter rulesAdapter = new ПравилоTableAdapter();
        static ФактПравилоTableAdapter factsRulesAdapter = new ФактПравилоTableAdapter();

        public static string LastLog
        {
            get;
            private set;
        }

        public static List<int> Run(List<int> facts)
        {
            StringBuilder log = new StringBuilder();

            // Load rules
            List<Rule> rules = new List<Rule>();
            foreach (var ruleRow in rulesAdapter.GetData())
            {
                Rule rule = new Rule();
                rule.mID = ruleRow.IDПравила;
                rule.mAntecendentFormula = ruleRow.ФормулаАтецендента;
                rule.mPriority = ruleRow.Приоритет;
                rule.mAntecendentExpression = new Expression(ruleRow.ФормулаАтецендента);

                foreach (var ruleFactRow in factsRulesAdapter.GetDataBy(rule.mID))
                {
                    if (ruleFactRow.ФактАтецендента)
                    {
                        rule.mAntecendentFacts.Add(ruleFactRow.IDФакта, ruleFactRow.ИдентификаторВФормуле);
                    }
                    else
                    {
                        rule.mConsecventFacts.Add(ruleFactRow.IDФакта);
                    }
                }

                rules.Add(rule);
            }

            // Resolve conflicts strategies of priority and antecendent length used
            rules = rules.OrderBy(rule => rule.mPriority)
                .ThenBy(rule => rule.mAntecendentFacts.Count).ToList();

            ////////////////////////
            // Main deduction loop
            ///////////////////////

            List<int> workingMemeory = new List<int>();
            workingMemeory.AddRange(facts);

            log.Append(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + " ");
            log.Append("Begin deduction. Working memory state: ");
            workingMemeory.ForEach(fact => log.Append(fact.ToString() + ", "));
            log.AppendLine();

            bool someRuleApplied = false;
            do
            {
                someRuleApplied = false;
                // Find first matching rule
                Rule matchingRule = null;
                foreach (Rule rule in rules)
                {
                    // Check, if rule match
                    foreach (var pair in rule.mAntecendentFacts)
                    {
                        int factID = pair.Key;
                        string factIDInFormula = pair.Value;
                        rule.mAntecendentExpression.Parameters[factIDInFormula] =
                            workingMemeory.Contains(factID);
                    }
                    try
                    {
                        if ((bool)rule.mAntecendentExpression.Evaluate())
                        {
                            matchingRule = rule;
                            break;
                        }
                    }
                    catch (Exception e)
                    {
                        log.AppendFormat("Failed to calculate antecendent for rule {0}. Reason: {1}.",
                            rule.mID, e.Message);
                        log.AppendLine();
                    }
                }

                if (matchingRule != null)
                {
                    log.AppendFormat("Applying rule {0}", matchingRule.mID);
                    log.AppendLine();

                    /*foreach (var pair in matchingRule.mAntecendentFacts)
                    {
                        workingMemeory.Remove(pair.Key);
                    }*/
                    workingMemeory.AddRange(matchingRule.mConsecventFacts);
                    rules.Remove(matchingRule);

                    log.Append("Iteration finished. Working memory state: ");
                    workingMemeory.ForEach(fact => log.Append(fact.ToString() + ", "));
                    log.AppendLine();
                    someRuleApplied = true;
                }

            } while (someRuleApplied);

            log.Append("There is not matching rules, finishing deduction");

            LastLog = log.ToString();

            return workingMemeory;
        }
    }
}
