﻿namespace OilExpert
{
    partial class TestInputSensors
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.iDДатчикаDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.расположениеDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.типDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Показание = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.датчикBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.oilDBDataSet = new OilExpert.OilDBDataSet();
            this.датчикTableAdapter = new OilExpert.OilDBDataSetTableAdapters.ДатчикTableAdapter();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonGo = new System.Windows.Forms.Button();
            this.показаниеДатчикаФактTableAdapter = new OilExpert.OilDBDataSetTableAdapters.ПоказаниеДатчикаФактTableAdapter();
            this.фактTableAdapter1 = new OilExpert.OilDBDataSetTableAdapters.ФактTableAdapter();
            this.зафиксированныйСбойTableAdapter1 = new OilExpert.OilDBDataSetTableAdapters.ЗафиксированныйСбойTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.датчикBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.oilDBDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDДатчикаDataGridViewTextBoxColumn,
            this.расположениеDataGridViewTextBoxColumn,
            this.типDataGridViewTextBoxColumn,
            this.Показание});
            this.dataGridView1.DataSource = this.датчикBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(12, 38);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(648, 284);
            this.dataGridView1.TabIndex = 0;
            // 
            // iDДатчикаDataGridViewTextBoxColumn
            // 
            this.iDДатчикаDataGridViewTextBoxColumn.DataPropertyName = "IDДатчика";
            this.iDДатчикаDataGridViewTextBoxColumn.HeaderText = "IDДатчика";
            this.iDДатчикаDataGridViewTextBoxColumn.Name = "iDДатчикаDataGridViewTextBoxColumn";
            this.iDДатчикаDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // расположениеDataGridViewTextBoxColumn
            // 
            this.расположениеDataGridViewTextBoxColumn.DataPropertyName = "Расположение";
            this.расположениеDataGridViewTextBoxColumn.HeaderText = "Расположение";
            this.расположениеDataGridViewTextBoxColumn.Name = "расположениеDataGridViewTextBoxColumn";
            this.расположениеDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // типDataGridViewTextBoxColumn
            // 
            this.типDataGridViewTextBoxColumn.DataPropertyName = "Тип";
            this.типDataGridViewTextBoxColumn.HeaderText = "Тип";
            this.типDataGridViewTextBoxColumn.Name = "типDataGridViewTextBoxColumn";
            this.типDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // Показание
            // 
            this.Показание.HeaderText = "Показание";
            this.Показание.Name = "Показание";
            // 
            // датчикBindingSource
            // 
            this.датчикBindingSource.DataMember = "Датчик";
            this.датчикBindingSource.DataSource = this.oilDBDataSet;
            // 
            // oilDBDataSet
            // 
            this.oilDBDataSet.DataSetName = "OilDBDataSet";
            this.oilDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // датчикTableAdapter
            // 
            this.датчикTableAdapter.ClearBeforeFill = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(272, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Введите тестовые показания датчиков:";
            // 
            // buttonGo
            // 
            this.buttonGo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonGo.Location = new System.Drawing.Point(499, 337);
            this.buttonGo.Name = "buttonGo";
            this.buttonGo.Size = new System.Drawing.Size(161, 32);
            this.buttonGo.TabIndex = 2;
            this.buttonGo.Text = "Запустить тест";
            this.buttonGo.UseVisualStyleBackColor = true;
            this.buttonGo.Click += new System.EventHandler(this.buttonGo_Click);
            // 
            // показаниеДатчикаФактTableAdapter
            // 
            this.показаниеДатчикаФактTableAdapter.ClearBeforeFill = true;
            // 
            // фактTableAdapter1
            // 
            this.фактTableAdapter1.ClearBeforeFill = true;
            // 
            // зафиксированныйСбойTableAdapter1
            // 
            this.зафиксированныйСбойTableAdapter1.ClearBeforeFill = true;
            // 
            // TestInputSensors
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(672, 381);
            this.Controls.Add(this.buttonGo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "TestInputSensors";
            this.Text = "Ввод тестовых показаний датчиков";
            this.Load += new System.EventHandler(this.TestInputSensors_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.датчикBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.oilDBDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private OilDBDataSet oilDBDataSet;
        private System.Windows.Forms.BindingSource датчикBindingSource;
        private OilDBDataSetTableAdapters.ДатчикTableAdapter датчикTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDДатчикаDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn расположениеDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn типDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Показание;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonGo;
        private OilDBDataSetTableAdapters.ПоказаниеДатчикаФактTableAdapter показаниеДатчикаФактTableAdapter;
        private OilDBDataSetTableAdapters.ФактTableAdapter фактTableAdapter1;
        private OilDBDataSetTableAdapters.ЗафиксированныйСбойTableAdapter зафиксированныйСбойTableAdapter1;
    }
}