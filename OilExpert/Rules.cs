﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OilExpert
{
    public partial class Rules : Form
    {
        OilDBDataSetTableAdapters.ФактПравилоViewTableAdapter фактПравилоViewTableAdapter =
            new OilDBDataSetTableAdapters.ФактПравилоViewTableAdapter();

        public Rules()
        {
            InitializeComponent();
        }

        private void Rules_Load(object sender, EventArgs e)
        {
            LoadRules();

        }

        private void LoadRules()
        {
            // TODO: This line of code loads data into the 'oilDBDataSet.Правило' table. You can move, or remove it, as needed.
            this.правилоTableAdapter.Fill(this.oilDBDataSet.Правило);
            правилоBindingSource.ResetBindings(false);

            // Fill "Атецендент" and "Консеквент" fileds of table 
            // by data from фактПравилоTableAdapter1
            foreach (var factRow in фактПравилоViewTableAdapter.GetData())
            {
                foreach (var ruleGridRowObj in dataGridView1.Rows)
                {
                    DataGridViewRow ruleGridRow = ruleGridRowObj as DataGridViewRow;
                    if ((int)ruleGridRow.Cells[0].Value == factRow.IDПравила)
                    {
                        if (factRow.ФактАтецендента)
                        {
                            string atecendent = Program.Convert<string>(ruleGridRow.Cells[1].Value, "");
                            if (atecendent.Length == 0)
                            {
                                // Cell 7 contains souce formula, with identifiers
                                atecendent = Program.Convert<string>(ruleGridRow.Cells[7].Value, "");
                            }
                            atecendent = atecendent.Replace(factRow.ИдентификаторВФормуле, factRow.Название);
                            ruleGridRow.Cells[1].Value = atecendent;
                        }
                        else
                        {
                            string consecvent = Program.Convert<string>(ruleGridRow.Cells[2].Value, "");
                            if (consecvent.Length > 0)
                            {
                                consecvent = consecvent + ", ";                                
                            }
                            consecvent = consecvent + factRow.Название;
                            ruleGridRow.Cells[2].Value = consecvent;
                        }
                    }
                }
            }
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            AddRule addWnd = new AddRule();
            if (addWnd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                // Insert into table Правило
                string author = Program.CurrentUser;
                DateTime creationTime = DateTime.Now;
                int priority = (int)addWnd.numericUpDownPriority.Value;
                string formulaOfAntecendent = addWnd.textBoxFormulaOfAntecendent.Text;
                правилоTableAdapter.Insert(priority, creationTime, formulaOfAntecendent, author);
                int ruleID = Program.Convert<int>(правилоTableAdapter.GetLastIsertedRuleID(), 0);

                // Insert child records into table ФактПравило
                foreach (var rowObj in addWnd.dataGridViewAntecendent.Rows)
                {
                    DataGridViewRow row = rowObj as DataGridViewRow;
                    int factID = (int)row.Cells[0].Value;
                    string factIDInFormula = (string)row.Cells[2].Value;
                    фактПравилоTableAdapter.Insert(factID, ruleID, factIDInFormula, true);
                }
                foreach (var rowObj in addWnd.dataGridViewConsecvent.Rows)
                {
                    DataGridViewRow row = rowObj as DataGridViewRow;
                    int factID = (int)row.Cells[0].Value;
                    фактПравилоTableAdapter.Insert(factID, ruleID, "", false);
                }

                // Update GUI
                LoadRules();
            }
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentRow != null)
            {
                if (MessageBox.Show(this, "Вы действительно хотите удалить правило?",
                "Подтверждение", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    правилоTableAdapter.Delete((int)dataGridView1.CurrentRow.Cells[0].Value);
                    LoadRules();
                }
            }
            else
            {
                MessageBox.Show("Выберите, пожалуйста, правило");
            }
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentRow != null)
            {

                AddRule addWnd = new AddRule();
                addWnd.Text = "Редактирование правила";
                addWnd.buttonOk.Text = "Сохранить";
                addWnd.numericUpDownPriority.Value = Program.Convert<int>(
                    dataGridView1.CurrentRow.Cells[3].Value, 0);
                addWnd.textBoxFormulaOfAntecendent.Text = Program.Convert<string>(
                    dataGridView1.CurrentRow.Cells[7].Value, "");
                int ruleID = Program.Convert<int>(
                    dataGridView1.CurrentRow.Cells[0].Value, 0);

                // Fill dataGridViewAntecendent and dataGridViewConsecvent
                foreach (var row in фактПравилоViewTableAdapter.GetDataBy1(ruleID))
                {
                    if (row.ФактАтецендента)
                    {
                        addWnd.dataGridViewAntecendent.Rows.Add(
                            row.IDФакта, row.Название, row.ИдентификаторВФормуле);
                    }
                    else
                    {
                        addWnd.dataGridViewConsecvent.Rows.Add(
                            row.IDФакта, row.Название);
                    }
                }

                if (addWnd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
            
                    // Insert into table Правило
                    string author = Program.Convert<string>(
                        dataGridView1.CurrentRow.Cells[6].Value, "");
                    DateTime creationTime = Program.Convert<DateTime>(
                        dataGridView1.CurrentRow.Cells[4].Value, DateTime.Now);
                    int priority = (int)addWnd.numericUpDownPriority.Value;
                    string formulaOfAntecendent = addWnd.textBoxFormulaOfAntecendent.Text;
                    правилоTableAdapter.Update(priority, creationTime, formulaOfAntecendent, author, ruleID);

                    // Delete old rule facts, and then insert new ones
                    фактПравилоViewTableAdapter.DeleteAllRuleFacts(ruleID);

                    // Insert child records into table ФактПравило
                    foreach (var rowObj in addWnd.dataGridViewAntecendent.Rows)
                    {
                        DataGridViewRow row = rowObj as DataGridViewRow;
                        int factID = (int)row.Cells[0].Value;
                        string factIDInFormula = (string)row.Cells[2].Value;
                        фактПравилоTableAdapter.Insert(factID, ruleID, factIDInFormula, true);
                    }
                    foreach (var rowObj in addWnd.dataGridViewConsecvent.Rows)
                    {
                        DataGridViewRow row = rowObj as DataGridViewRow;
                        int factID = (int)row.Cells[0].Value;
                        фактПравилоTableAdapter.Insert(factID, ruleID, "", false);
                    }

                    // Update GUI
                    LoadRules();
                }
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            Close();
        }

    }
}
