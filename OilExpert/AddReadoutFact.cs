﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OilExpert
{
    public partial class AddReadoutFact : Form
    {
        public int mFactToSelectAfterShow = -1;

        public AddReadoutFact()
        {
            InitializeComponent();
        }

        private void AddReadoutFact_Load(object sender, EventArgs e)
        {
            LoadFacts();
            // Select mFactToSelectAfterShow 
            foreach (var rowObj in dataGridViewFacts.Rows)
            {
                DataGridViewRow row = rowObj as DataGridViewRow;
                if ((int)row.Cells[0].Value == mFactToSelectAfterShow)
                {
                    row.Selected = true;
                }
                else
                {
                    row.Selected = false;
                }
            }
        }

        public void LoadFacts()
        {
            // TODO: This line of code loads data into the 'oilDBDataSet.Факт' table. You can move, or remove it, as needed.
            this.фактTableAdapter.Fill(this.oilDBDataSet.Факт);
            фактBindingSource.ResetBindings(false);
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
            Hide();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            Hide();
        }
    }
}
