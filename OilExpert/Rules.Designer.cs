﻿namespace OilExpert
{
    partial class Rules
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.buttonEdit = new System.Windows.Forms.Button();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.правилоBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.oilDBDataSet = new OilExpert.OilDBDataSet();
            this.правилоTableAdapter = new OilExpert.OilDBDataSetTableAdapters.ПравилоTableAdapter();
            this.фактПравилоTableAdapter = new OilExpert.OilDBDataSetTableAdapters.ФактПравилоTableAdapter();
            this.iDПравилаDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Атецендент = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Консеквент = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.приоритетDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.времяСозданияDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.формулаАтецендентаDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.логинАвтораDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ФормулаАтецендента = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonOk = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.правилоBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.oilDBDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonRemove
            // 
            this.buttonRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonRemove.Location = new System.Drawing.Point(304, 308);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(97, 28);
            this.buttonRemove.TabIndex = 14;
            this.buttonRemove.Text = "Удалить";
            this.buttonRemove.UseVisualStyleBackColor = true;
            this.buttonRemove.Click += new System.EventHandler(this.buttonRemove_Click);
            // 
            // buttonEdit
            // 
            this.buttonEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonEdit.Location = new System.Drawing.Point(169, 308);
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Size = new System.Drawing.Size(128, 28);
            this.buttonEdit.TabIndex = 13;
            this.buttonEdit.Text = "Редактировать";
            this.buttonEdit.UseVisualStyleBackColor = true;
            this.buttonEdit.Click += new System.EventHandler(this.buttonEdit_Click);
            // 
            // buttonAdd
            // 
            this.buttonAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAdd.Location = new System.Drawing.Point(10, 308);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(153, 28);
            this.buttonAdd.TabIndex = 12;
            this.buttonAdd.Text = "Добавить";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDПравилаDataGridViewTextBoxColumn,
            this.Атецендент,
            this.Консеквент,
            this.приоритетDataGridViewTextBoxColumn,
            this.времяСозданияDataGridViewTextBoxColumn,
            this.формулаАтецендентаDataGridViewTextBoxColumn,
            this.логинАвтораDataGridViewTextBoxColumn,
            this.ФормулаАтецендента});
            this.dataGridView1.DataSource = this.правилоBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(10, 8);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(754, 282);
            this.dataGridView1.TabIndex = 11;
            // 
            // правилоBindingSource
            // 
            this.правилоBindingSource.DataMember = "Правило";
            this.правилоBindingSource.DataSource = this.oilDBDataSet;
            // 
            // oilDBDataSet
            // 
            this.oilDBDataSet.DataSetName = "OilDBDataSet";
            this.oilDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // правилоTableAdapter
            // 
            this.правилоTableAdapter.ClearBeforeFill = true;
            // 
            // фактПравилоTableAdapter
            // 
            this.фактПравилоTableAdapter.ClearBeforeFill = true;
            // 
            // iDПравилаDataGridViewTextBoxColumn
            // 
            this.iDПравилаDataGridViewTextBoxColumn.DataPropertyName = "IDПравила";
            this.iDПравилаDataGridViewTextBoxColumn.HeaderText = "IDПравила";
            this.iDПравилаDataGridViewTextBoxColumn.Name = "iDПравилаDataGridViewTextBoxColumn";
            this.iDПравилаDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDПравилаDataGridViewTextBoxColumn.Visible = false;
            // 
            // Атецендент
            // 
            this.Атецендент.FillWeight = 200F;
            this.Атецендент.HeaderText = "Атецендент";
            this.Атецендент.Name = "Атецендент";
            this.Атецендент.ReadOnly = true;
            // 
            // Консеквент
            // 
            this.Консеквент.FillWeight = 200F;
            this.Консеквент.HeaderText = "Консеквент";
            this.Консеквент.Name = "Консеквент";
            this.Консеквент.ReadOnly = true;
            // 
            // приоритетDataGridViewTextBoxColumn
            // 
            this.приоритетDataGridViewTextBoxColumn.DataPropertyName = "Приоритет";
            this.приоритетDataGridViewTextBoxColumn.FillWeight = 70F;
            this.приоритетDataGridViewTextBoxColumn.HeaderText = "Приоритет";
            this.приоритетDataGridViewTextBoxColumn.Name = "приоритетDataGridViewTextBoxColumn";
            this.приоритетDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // времяСозданияDataGridViewTextBoxColumn
            // 
            this.времяСозданияDataGridViewTextBoxColumn.DataPropertyName = "ВремяСоздания";
            this.времяСозданияDataGridViewTextBoxColumn.FillWeight = 60F;
            this.времяСозданияDataGridViewTextBoxColumn.HeaderText = "Время создания";
            this.времяСозданияDataGridViewTextBoxColumn.Name = "времяСозданияDataGridViewTextBoxColumn";
            this.времяСозданияDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // формулаАтецендентаDataGridViewTextBoxColumn
            // 
            this.формулаАтецендентаDataGridViewTextBoxColumn.DataPropertyName = "ФормулаАтецендента";
            this.формулаАтецендентаDataGridViewTextBoxColumn.HeaderText = "ФормулаАтецендента";
            this.формулаАтецендентаDataGridViewTextBoxColumn.Name = "формулаАтецендентаDataGridViewTextBoxColumn";
            this.формулаАтецендентаDataGridViewTextBoxColumn.ReadOnly = true;
            this.формулаАтецендентаDataGridViewTextBoxColumn.Visible = false;
            // 
            // логинАвтораDataGridViewTextBoxColumn
            // 
            this.логинАвтораDataGridViewTextBoxColumn.DataPropertyName = "ЛогинАвтора";
            this.логинАвтораDataGridViewTextBoxColumn.FillWeight = 50F;
            this.логинАвтораDataGridViewTextBoxColumn.HeaderText = "Автор";
            this.логинАвтораDataGridViewTextBoxColumn.Name = "логинАвтораDataGridViewTextBoxColumn";
            this.логинАвтораDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // ФормулаАтецендента
            // 
            this.ФормулаАтецендента.DataPropertyName = "ФормулаАтецендента";
            this.ФормулаАтецендента.HeaderText = "ИсходнаяФормулаАтецендента";
            this.ФормулаАтецендента.Name = "ФормулаАтецендента";
            this.ФормулаАтецендента.ReadOnly = true;
            this.ФормулаАтецендента.Visible = false;
            // 
            // buttonOk
            // 
            this.buttonOk.Location = new System.Drawing.Point(641, 308);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(123, 28);
            this.buttonOk.TabIndex = 15;
            this.buttonOk.Text = "ОК";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // Rules
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(771, 348);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.buttonRemove);
            this.Controls.Add(this.buttonEdit);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Rules";
            this.Text = "Правила";
            this.Load += new System.EventHandler(this.Rules_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.правилоBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.oilDBDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonRemove;
        private System.Windows.Forms.Button buttonEdit;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.DataGridView dataGridView1;
        private OilDBDataSet oilDBDataSet;
        private System.Windows.Forms.BindingSource правилоBindingSource;
        private OilDBDataSetTableAdapters.ПравилоTableAdapter правилоTableAdapter;
        private OilDBDataSetTableAdapters.ФактПравилоTableAdapter фактПравилоTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDПравилаDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Атецендент;
        private System.Windows.Forms.DataGridViewTextBoxColumn Консеквент;
        private System.Windows.Forms.DataGridViewTextBoxColumn приоритетDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn времяСозданияDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn формулаАтецендентаDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn логинАвтораDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ФормулаАтецендента;
        private System.Windows.Forms.Button buttonOk;
    }
}